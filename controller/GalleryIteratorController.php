<?php

namespace controller;

use model\GalleryIterator;
use model\Extension;
use model\Directory;

class GalleryIteratorController
{
    private GalleryIterator $gallery_iterator;
    private Extension $extension;

    private array $jpgs;
    private array $movs;
    private array $aaes;
    private array $heics;

    public function __construct()
    {
        $this->gallery_iterator = new GalleryIterator();
        $this->extension = new Extension();
    }

    public function printIteratedFiles(): void
    {
        foreach ($this->gallery_iterator->getAllFiles() as $item) {
            if ((substr($item, -1) != '.') && (!is_dir($item))) {
                echo $item . '<br>';
            }
        }
    }

    public function downloadFilesToLocal()
    {
        // parts by 19-20 elements because of problems with download to PC
        $files = $this->gallery_iterator->getAllFiles();
        $max = iterator_to_array($files);
        var_dump($files);
        $max = count($files[0]);
        $iterate_to = 20;
        $offset = 20;
        $i = 0;
        // $timer = new DateTime();
        while ($i < $max) {
            for ($i; $i < $iterate_to || $i < $max; $i++) {
                // $new_address = '...local file system/...' . '...file_name...';
                // copy($files[$i], $new_address);
                var_dump($files->splFileInfo[0]);
                // $files_on_local[] = $new_address;
            }
            // maybe it'll help with disconnecting from PC
            sleep(3);
            $iterate_to += $offset;
        }
    }

    private function divideFilesBetweenArrays(): bool
    {
        // CHANGE ITERATEIPHONEGALLERY() TO ITERATE OVER LOCAL DIR (?)
        foreach ($this->gallery_iterator->getAllFiles() as $file) {
            if ($this->extension->getExtension($file) === 'jpg') {
                $this->jpgs[] = substr($file, 0, -4);
            } elseif ($this->extension->getExtension($file) === 'mov') {
                $this->movs[] = substr($file, 0, -4);
            } elseif ($this->extension->getExtension($file) === 'aae') {
                $this->aaes[] = substr($file, 0, -4);
            } elseif ($this->extension->getExtension($file) === 'heic') {
                $this->heics[] = substr($file, 0, -5);
            } else {
                // move files to unknown_files/ directory (create it first in Directory.php)
                // or even better - do nothing with them
            }
        }
        return true;
    }

    private function getStandardMovies(): bool
    {
        // keep movs which don't have corresponding jpgs with same name
        // because these are normal movie files (not live photos)
        if ($this->gallery_iterator->divideFilesBetweenArrays()) {
            return array_diff($this->jpgs, $this->movs);
        }
        return false;
    }

    private function renameStandardMovies(): void
    {
        foreach ($this->getStandardMovies() as $file) {
                // note that extension was cropped before
                // to easily compare photos and livephotos by name
                rename($file . 'mov', $this->dir->getMovDestDir() . $file . 'mov');
                $this->movs = array_diff($this->movs, $file);
        }
    }

    private function unlinkLivePhotos(): void
    {
        // standard movies were renamed before in renameStandardMobies()
        // so movs[] contains just mov files which are livephotos
        // if jpg and mov have same names, then unlink mov
        foreach ($this->movs as $file) {
            unlink($file);
        }
    }
}
