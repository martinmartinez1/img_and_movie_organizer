<?php

namespace controller;

use model\Notification;
use model\Directory;

class NotificationController
{
    private Notification $notification;
    private Directory $dir;

    public function __construct()
    {
        $this->notification = new Notification();
        $this->dir = new Directory();
    }

    public function getImgSuccessNote(): string
    {
        return $this->notification->getImageSuccessNote($this->dir->getImgDestDir(), $this->dir->getDuplicatesDir());
    }

    public function getImgErrorNote(): string
    {
        return $this->notification->getImageErrorNote($this->dir->getSrcDir());
    }

    public function getMovSuccessNote(): string
    {
        return $this->notification->getMovieSuccessNote($this->dir->getMovDestDir(), $this->dir->getDuplicatesDir());
    }

    public function getMovErrorNote(): string
    {
        return $this->notification->getMovieErrorNote($this->dir->getSrcDir());
    }
}
