<?php

namespace model;

class Button
{
    private string $run = 'run.php';
    private string $index = 'index.php';

    public function getRunButton()
    {
        return '
        <a href=' . $this->run . '>
            <button>
                PERFORM SCAN
            </button>
        </a>
        ';
    }

    public function getIndexButton()
    {
        return '
        <a href=' . $this->index . '>
            <button>
                Back to info page
            </button>
        </a>
        ';
    }
}
