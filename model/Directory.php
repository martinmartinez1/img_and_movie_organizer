<?php

namespace model;

class Directory
{
    private string $user_files_dir = 'your_files/';
    private string $src_dir = 'src/';
    private string $img_dest_dir = 'img_renamed/';
    private string $mov_dest_dir = 'mov_renamed/';
    private string $duplicates_dir = 'duplicates/';

    private string $start_path_to_be_scanned = '0';

    public function createDirectories(): bool
    {
        if (!is_dir($this->getSrcDir())) {
            mkdir($this->getSrcDir(), 0777, true);
        }
        if (!is_dir($this->getImgDestDir())) {
            mkdir($this->getImgDestDir(), 0777, true);
        }
        if (!is_dir($this->getMovDestDir())) {
            mkdir($this->getMovDestDir(), 0777, true);
        }
        if (!is_dir($this->getDuplicatesDir())) {
            mkdir($this->getDuplicatesDir(), 0777, true);
        }
        return true;
    }

    public function getSrcDir(): string
    {
        return $this->user_files_dir . $this->src_dir;
    }

    public function getImgDestDir(): string
    {
        return $this->user_files_dir . $this->img_dest_dir;
    }

    public function getMovDestDir(): string
    {
        return $this->user_files_dir . $this->mov_dest_dir;
    }

    public function getDuplicatesDir(): string
    {
        return $this->user_files_dir . $this->duplicates_dir;
    }

    public function getStartPathToBeScanned(): string
    {
        return $this->start_path_to_be_scanned;
    }

    public function setStartPathToBeScanned($start_path_to_be_scanned): void
    {
        $this->start_path_to_be_scanned = $start_path_to_be_scanned;
    }
}
