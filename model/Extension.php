<?php

namespace model;

class Extension
{
    private array $allowed_img_extensions = [
        'gif',
        'jpeg',
        'jpg',
        'png',
        'bmp',
        'webp',
        'ico'
    ];

    private array $allowed_mov_extensions = [
        '3gp',
        'avchd',
        'avi',
        'flv',
        'm4p',
        'm4v',
        'mov',
        'mp2',
        'mp4',
        'mpeg',
        'mpe',
        'mpg',
        'mpv',
        'ogg',
        'qt',
        'rmvb',
        'vlc',
        'webm',
        'wmv'
    ];

    private array $allowed_iphone_extensions = [
        'jpg',
        'mov',
        'aae',
        'heic'
    ];

    public function getExtension($file): string
    {
        return strtolower(pathinfo($file, PATHINFO_EXTENSION));
    }

    public function getAllowedImgExtensions(): array
    {
        return $this->allowed_img_extensions;
    }

    public function getAllowedMovExtensions(): array
    {
        return $this->allowed_mov_extensions;
    }

    public function getAllowedIPhoneExtensions(): array
    {
        return $this->allowed_iphone_extensions;
    }
}
