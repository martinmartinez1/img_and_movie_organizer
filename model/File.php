<?php

namespace model;

abstract class File
{
    protected string $file;
    protected string $new_name;
    protected array $files_found = [];

    protected Directory $dir;

    protected Extension $ext;

    public function __construct()
    {
        $this->dir = new Directory();
        $this->ext = new Extension();
    }

    abstract protected function renameFile(): bool;

    protected function getFilePath(): string
    {
        return $this->dir->getSrcDir() . $this->file;
    }

    // protected function getExtension(): string
    protected function getExtension($file): string
    {
        // return strtolower(pathinfo($this->getFilePath(), PATHINFO_EXTENSION));
        return strtolower(pathinfo($file, PATHINFO_EXTENSION));
    }

    protected function getModificationTime(): string
    {
        return gmdate("Y-m-d H-i-s", filemtime($this->file));
    }

    protected function createNewName($date_time): string
    {
        return preg_replace('/[:-]/', '_', $date_time) . '.' . $this->getExtension($this->file);
    }

    protected function listFiles($extension_group): array
    {
        $this_dir_files = scandir($this->dir->getSrcDir());
        foreach ($this_dir_files as $this->file) {
            if (
                is_file($this->getFilePath())
                && in_array($this->getExtension($this->getFilePath()), $extension_group)
            ) {
                $this->files_found[] = $this->getFilePath();
            }
        }
        return $this->files_found;
    }

    protected function moveFile($file_group_dest_dir): bool
    {
        if (
            file_exists($file_group_dest_dir . $this->new_name)
            && sha1_file($file_group_dest_dir . $this->new_name) === sha1_file($this->file)
        ) {
            if (!file_exists($this->dir->getDuplicatesDir() . $this->new_name)) {
                rename($this->file, $this->dir->getDuplicatesDir() . $this->new_name);
                return true;
            } else {
                unlink($this->file);
                return true;
            }
        } elseif (
            file_exists($file_group_dest_dir . $this->new_name)
            && sha1_file($file_group_dest_dir . $this->new_name) !== sha1_file($this->file)
        ) {
            $ext = substr($this->new_name, -(strlen('.' . $this->getExtension($this->file))));
            $temp_name = rtrim($this->new_name, $ext);
            $this->new_name = $temp_name . '(n)' . $ext;
            rename($this->file, $file_group_dest_dir . $this->new_name);
            return true;
        }
        rename($this->file, $file_group_dest_dir . $this->new_name);
        return true;
    }
}
