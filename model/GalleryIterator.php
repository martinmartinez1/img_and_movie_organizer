<?php

namespace model;

use model\Directory;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

class GalleryIterator
{
    private Directory $dir;

    public function __construct()
    {
        $this->dir = new Directory();
    }

    public function getAllFiles(): RecursiveIteratorIterator
    {
        return new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->dir->getStartPathToBeScanned()), 1);
    }
}
