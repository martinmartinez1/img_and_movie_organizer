<?php

namespace model;

use model\File;

class Image extends File
{
    // private function getExifTime($file)
    // {
    //     return exif_read_data($file['DateTimeOriginal']);
    // }

    public function renameFile(): bool
    {
        if (empty($this->listFiles($this->ext->getAllowedImgExtensions()))) {
            return false;
        } else {
            foreach ($this->files_found as $this->file) {
                // if (
                //     isset(exif_read_data($this->getFilePath())['DateTimeOriginal'])
                //     && filesize($this->file) > 11
                // ) {
                //     $this->new_name = $this->createNewName($this->getExifTime($file));
                //     $this->moveFile($this->dir->getImgDestDir());
                // } else {
                    $this->new_name = $this->createNewName($this->getModificationTime());
                    $this->moveFile($this->dir->getImgDestDir());
                // }
            }
            return true;
        }
    }
}
