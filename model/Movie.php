<?php

namespace model;

use model\File;

class Movie extends File
{
    public function renameFile(): bool
    {
        if (empty($this->listFiles($this->ext->getAllowedMovExtensions()))) {
            return false;
        } else {
            foreach ($this->files_found as $this->file) {
                $this->new_name = $this->createNewName($this->getModificationTime());
                $this->moveFile($this->dir->getMovDestDir());
            }
            return true;
        }
    }
}
