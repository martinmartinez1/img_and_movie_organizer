<?php

namespace model;

use model\Directory;

class Notification
{
    private Directory $dir;

    public function __construct()
    {
        $this->dir = new Directory();
    }

    public function getImageSuccessNote($img_dest_dir, $img_duplicates_dir): string
    {
        return "Images have new names.<br>
        You can find them in {$this->dir->getImgDestDir()}<br>
        Check for duplicates in {$this->dir->getDuplicatesDir()} to be sure these can be removed (no automation here)";
    }

    public function getImageErrorNote(): string
    {
        return "It looks like there was no images in {$this->dir->getSrcDir()}(?)";
    }

    public function getMovieSuccessNote(): string
    {
        return "Movies have new names.<br>
        You can find them in {$this->dir->getMovDestDir()}<br>
        Check for duplicates in {$this->dir->getDuplicatesDir()} to be sure these can be removed (no automation here)";
    }

    public function getMovieErrorNote(): string
    {
        return "It looks like there was no videos in {$this->dir->getSrcDir()}(?)";
    }
}
