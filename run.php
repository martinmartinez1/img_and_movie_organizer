<?php

use model\Image;
use model\Movie;
use model\Button;
use model\Directory;
use controller\NotificationController;

require_once 'template_parts/header.php';

$image = new Image();
$movie = new Movie();
$button = new Button();
$dir = new Directory();
$notification_controller = new NotificationController();

?>
<h3>Img stuff:</h3>
<?php
if (true === $image->renameFile()) {
    echo $notification_controller->getImgSuccessNote();
} else {
    echo $notification_controller->getImgErrorNote();
}
?>
<h3>Mov stuff:</h3>
<?php
if (true === $movie->renameFile()) {
    echo $notification_controller->getMovSuccessNote();
} else {
    echo $notification_controller->getMovErrorNote();
}
?>

<br>
<?php require_once 'template_parts/directory_choose.php' ?>
<br>
<?= $button->getRunButton(); ?>
<?= $button->getIndexButton(); ?>

<?php

require_once 'template_parts/footer.php';
