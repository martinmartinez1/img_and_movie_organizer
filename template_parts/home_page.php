<?php

use model\Button;

$button = new Button();

?>
<h4>Image and movie renaming</h4>
<p>
    With this tool you can easy rename and sort by date your images and movies.
</p>
<p>
    It happens that files from diferent cameras have diferent names
    and then are hard to find because of default sorting on your OS.
</p>
<p>
    And if you have found that there are multiple backups with same files
    on your device so here you get duplicates just once for check.
</p>
<p>
    When two files has the same new_name (modification date), but different size,
    then one of them get n * "n" appended to its name. The new one will not be deleted first. First append, next moves to duplicates.
</p>
<p>
    After that 3rd duplicated file will be just removed.
</p>
<p>
    In next versions:
    <ul>
        <li>Making an exe for local usage</li>
    </ul>
    Changelog for main tasks:
    <ul>
        <li>Checking for size in addition to just date</li>
        <li>deprecated: Using som external lib for movies</li>
    </ul>

</p>
<br>
<div class="alert">
<p>How it works</p>
<ul>
    <li>Move or copy files to <?= $dir->getSrcDir(); ?> directory</li>
    <li>Push "RUN" button</li>
    <li>First occurance come up in <?= $dir->getImgDestDir() ?> and <?= $dir->getMovDestDir() ?></li>
    <li>Second occurance come up in <?= $dir->getDuplicatesDir() ?></li>
    <li>Third occurance will be removed</li>
    <li>Move renamed files wherever you want</li>
    <li>Check if everything's ok in destination folder</li>
    <li>Now you can clean all mess you've got from the past</li>
</ul>
</div>

<br>

<h3>
    I've read and understood that success depends on some circumstances and this tool is first of all for test purposes.
</h3>
<?= $button->getRunButton(); ?>
